import 'package:flutter/material.dart';
import 'package:flutter_qr_bar_scanner/qr_bar_scanner_camera.dart';
import 'package:miniments/FadeIn.dart';

class Scanner extends StatefulWidget {
  @override
  _ScannerState createState() => _ScannerState();
}

class _ScannerState extends State<Scanner> {
  String _qrInfo = 'Scan a QR/Bar code';
  bool _camState = false;

  _qrCallback(String code) {
    setState(() {
      _camState = false;
      _qrInfo = code;
    });
  }

  String _getImageByCode(String qrCode)
  {
    List<String> splitSring = qrCode.split("/");

    return splitSring[3] + ".jpeg";
  }

  _scanCode() {
    setState(() {
      _camState = true;
    });
  }

  @override
  void initState() {
    super.initState();
    _scanCode();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        leading: FlatButton(
          child: Image.asset("assets/buttons/scanButton.png"),
          onPressed: () => Navigator.of(context).pop(),
        ), 
        title: Padding(
          padding: EdgeInsets.only(left : 75.0),
          child: Text("Miniments",
            style: TextStyle(color : Colors.black),
            ),
        ),
        backgroundColor: Colors.white,
      ),
      body: _camState ?
        Center(
            child: SizedBox(
              height: 500,
              width: 500,
              child: QRBarScannerCamera(
                onError: (context, error) => Text(
                  error.toString(),
                  style: TextStyle(color: Colors.red),
                ),
                qrCodeCallback: (code) {
                  _qrCallback(code);
                },

              ),
            ),
          )
        :
        Stack(
          children: <Widget>[
            Image.asset('assets/pictures/logo.png'),
            Center(
              child: FadeIn(
                child: Image.asset("assets/memories/" + _getImageByCode(_qrInfo)),
                fadeInDuration: Duration(milliseconds: 1500),
                delay: Duration(seconds: 1),
              )
            ),
          ],
      )
    );
  }
}