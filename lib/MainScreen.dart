import 'package:flutter/material.dart';
import 'package:miniments/Scanner.dart';
import 'package:miniments/Collections.dart';

class MainScreen extends StatelessWidget {

  switchToScanner(){

  }

  @override
  Widget build(BuildContext context) {

    double screenHeigth = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      body: Center(
          child : Column(
          children: <Widget>[
            Container(
              child: Image.asset('assets/pictures/minimentText.png'),
              alignment: Alignment.center,
              margin: EdgeInsets.fromLTRB(0, screenHeigth * 0.2, 0, screenHeigth * 0.1),
              width: screenWidth * 0.7,
            ),
            Container(
              width: screenWidth * 0.75,
              padding: EdgeInsets.only(bottom : screenHeigth * 0.1),
              child: FlatButton(
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => Scanner()));
                },
                child: Image.asset(
                  "assets/buttons/scanButton.png"
                ) 
              ),
            ),
            Container(
              child: FlatButton(
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => Collections()));
                },
                child: const Text("Switch To Previous Tab")),
            )
          ],
          crossAxisAlignment: CrossAxisAlignment.center,
        ),
      )
    );
  }
}