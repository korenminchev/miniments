import 'package:flutter/material.dart';
import 'package:miniments/VideoPlayer.dart';

class WelcomeScreen extends StatelessWidget {

  final TabController _controller;

  WelcomeScreen(this._controller);

  @override
  Widget build(BuildContext context) {

    double screenHeigth = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      body: Center(
      child : Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(0, screenHeigth * 0.05, 0, screenHeigth * 0.015),
            height: screenHeigth * 0.55,
            child: Image.asset('assets/pictures/welcome.png'),
          ),
          Container(
            height: screenHeigth * 0.30,
            padding: EdgeInsets.only(bottom: screenHeigth * 0.015),
            child: VideoDemo('assets/videos/promo.mp4'),
          ),
          Container(
            child: FlatButton(
              child: Image.asset('assets/buttons/start.png'),
              onPressed: (){
                _controller.animateTo(1);
              },
            ),
            width: screenWidth * 0.55,
            height: screenHeigth * 0.1,
          )
        ],
      ),
      )      
    );
  }
}