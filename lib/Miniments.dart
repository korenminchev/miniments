import 'package:flutter/material.dart';
import 'package:miniments/WelcomeScreen.dart';
import 'package:miniments/MainScreen.dart';

class Miniments extends StatefulWidget {
  @override
  _MinimentsState createState() => _MinimentsState();
}

class _MinimentsState extends State<Miniments> with TickerProviderStateMixin{

  TabController _tabController;
  static final Padding onIcon = Padding(
    child : Icon(Icons.brightness_1, color: Colors.grey[600], size : 12),
    padding : EdgeInsets.fromLTRB(10, 0, 10, 0));
  static final Padding offIcon = Padding(
    child : Icon(Icons.brightness_1, color: Colors.grey[400], size : 12),
    padding : EdgeInsets.fromLTRB(10, 0, 10, 0));
  List<Widget> points = [
    onIcon, offIcon, offIcon
  ];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 3);
    _tabController.addListener(() {
      setState(() {
        for(int i = 0; i < _tabController.length; i++)
        {
          if(i == _tabController.index)
          {
            points[i] = onIcon;
          }
          else
          {
            points[i] = offIcon;
          }
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          body: TabBarView(
            controller: _tabController,
            children: [
              WelcomeScreen(_tabController),
              MainScreen(),
              Icon(Icons.directions_bike),
            ],
          ),
          bottomNavigationBar: BottomAppBar(
            elevation: 0,
            color: Colors.white10,
            child :Padding( 
              padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
              child :Row(
                children: points,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
              )
            ),
          ),
        ),
      ),
    );
  }
}

