import 'package:flutter/material.dart';

class FadeIn extends StatefulWidget {
  final Widget child;
  final Duration fadeInDuration;
  final Duration delay;

  FadeIn({@required this.child, @required this.fadeInDuration, this.delay = const Duration(seconds: 0)});

  @override
  _FadeInState createState() => _FadeInState();
}

class _FadeInState extends State<FadeIn> 
  with SingleTickerProviderStateMixin{

  AnimationController _controller;
  Animation _animation;

  @override
  void initState(){
    _controller = AnimationController(
      vsync: this,
      duration: widget.fadeInDuration,
    );

    _animation = Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(_controller);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void startControllerDelayed(AnimationController controller, Duration delay) async
  {
    await Future.delayed(delay);
    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    startControllerDelayed(_controller, widget.delay);
    return FadeTransition(
      opacity: _animation,
      child: widget.child,
    );
  }
}