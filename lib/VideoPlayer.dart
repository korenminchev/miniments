import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
 
class VideoDemo extends StatefulWidget {
  VideoDemo(this.asset) : super();
 
  final String asset;

  @override
  VideoDemoState createState() => VideoDemoState(asset);
}
 
class VideoDemoState extends State<VideoDemo> {
  
  VideoDemoState(this.asset);

  final String asset;

  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;
 
  @override
  void initState() {
    _controller = VideoPlayerController.asset(asset);
    _initializeVideoPlayerFuture = _controller.initialize();
    _controller.setLooping(true);
    _controller.setVolume(0.0);
    _controller.play();
    super.initState();
  }
 
  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
 
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _initializeVideoPlayerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return Center(
              child: AspectRatio(
                aspectRatio: _controller.value.aspectRatio,
                child: VideoPlayer(_controller),
              ),
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      );
  }
}